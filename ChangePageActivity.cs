﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using Android.Widget;
using LoginScreenAndroidApp.Resources;

namespace LoginScreenAndroidApp
{
    [Activity(Label = "newLayout", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class ChangePageActivity : AppCompatActivity
    {
        public  static int ResultCode = 2;

        TextView edit;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

           edit = FindViewById<EditText>(Resource.Id.txtvew);
            // Create your application here
           
            SetContentView(Resource.Layout.NewLayout);

            //android.support.v7.widget.toolbar tool = findviewbyid<android.support.v7.widget.toolbar>(resource.id.toolbar1);
            //setsupportactionbar(tool);

            Android.Support.V7.Widget.Toolbar tool = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar1);
            
            
            SetSupportActionBar(tool);

           Button b = FindViewById<Button>(Resource.Id.button1);

            // TextView text = FindViewById<TextView>(Resource.Id.textWelcome);

            // string usermane = Intent.GetStringExtra("userName");

            //  text.Text = "Welcome " + usermane;

          //  string text = "<font color = #FF0000>Hello </font> <font color = #FF0FFFF> world</font>";

            //this.edit.SetText(Html.FromHtml(GetString(Resource.String.myText),FromHtmlOptions.ModeCompact);


            var lstVew = FindViewById<ListView>(Resource.Id.listView);

            
            b.Click += (obj, e) =>
            {

                //Intent i = new Intent(this, typeof(PersonalActivity));

                //StartActivity(i);

                //StartActivityForResult(typeof(PersonalActivity), ResultCode);


                List<Information> data = new List<Information>
            {
                new Information{UpdateDate = "Prabhat",Id= 0,Time="21",By="123@123.cc"},
                new Information{UpdateDate = "arif",Id= 1,Time="21",By="123@123.cc"},
                new Information{UpdateDate= "Ashish",Id= 2,Time="21",By="123@123.cc"},
                new Information{UpdateDate = "Ayush",Id= 3,Time="21",By="123@123.cc"}
            };

                var adapter = new CustomAdapter(this, data);

                lstVew.Adapter = adapter;


            };



        }
        //public override bool (imenu menu)
        //{
        //    var inflater = menuinflater;
        //    inflater.inflate(resource.layout.menulayout, menu);
        //    return base.oncreateoptionsmenu(menu);
        //}

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var inflater = MenuInflater;
            inflater.Inflate(Resource.Menu.MenuLayout, menu);
            return base.OnCreateOptionsMenu(menu);
        }


        //public override bool OnOptionsItemSelected(IMenuItem item)
        //{

        //        Int id = item.ItemId; // getting i.d of the selected item  
        //        If(id == Resource.Id.search) {
        //            Toast.MakeText(this, ”Search clicked”, ToastLength.Short).Show();
        //            return true;
        //        }
        //        If(id == Resource.Id.share) {
        //            Toast.MakeText(this, ”Share clicked”, ToastLength.Short).Show();
        //            return true;
        //        }
        //        If(id == Resource.Id.email) {
        //            Toast.MakeText(this, ”Email clicked”, ToastLength.Short).Show();
        //            return true;
        //        }
        //        Return base.OnOptionsItemSelected(item);



        //}
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            edit = FindViewById<TextView>(Resource.Id.txtvew);
            // base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == ResultCode)
                if (resultCode == Result.Ok)
                    edit.Text = data.Data.ToString();
        }
    }
}
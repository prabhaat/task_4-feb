﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SqlConnectionApp.Data;
using System.IO;

namespace SqlConnectionApp
{
    public partial class App : Application
    {
        static Database database;

        public static Database Datas
        {
            get{
                if(database == null)
                {
                    database = new Database
                                (Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "SqlConnectionApp.db1"));
                }
                return database;
                         
            }
        }
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}

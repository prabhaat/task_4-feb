﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace SqlConnectionApp.Model
{
    public class Column
    {
        [PrimaryKey,AutoIncrement]
        public int Id { get; set; }

        public string StationName { get; set; }
        public string Colour { get; set; }
    }
}

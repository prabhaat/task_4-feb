﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SqlConnectionApp.Model;
using SQLite;

namespace SqlConnectionApp.Data
{
    public class Database
    {
        readonly SQLiteAsyncConnection _database;

        //===================================
        //    Create Connection with DataBase
        //===================================
        public Database(string path)
        {
            _database = new SQLiteAsyncConnection(path);   //pass the path for which you want connection

            _database.CreateTableAsync<Column>().Wait();
        }

        //===================================
        //    Retrieving from database from 
        //=====================================
        public Task<List<Column>> GetDataAsync()
        {
            return _database.Table<Column>().ToListAsync();
        }

        //======================================
        //  Retrieving data for specific Id
        //==========================================
        public Task<Column> GetDataAsync(int id)
        {
            return _database.Table<Column>().Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        //=========================================
        //  Inserting into DataBase
        //=========================================
        public Task<int> InsertData(Column column)
        {
            //if(column.Id!=0)
            //{
            //    return _database.UpdateAsync(column);
            //}
            return _database.InsertAsync(column);
        }
    }
}

﻿using SqlConnectionApp.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SqlConnectionApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public static int i = 0;
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new Column();
        }

        protected override async void OnAppearing()
        {
           
            listView.ItemsSource = await App.Datas.GetDataAsync();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            if(i%2 == 0)
            {
                listView.IsVisible = true;
            }
            else
            {
                listView.IsVisible = false;
            }

            i++;
            
        }

        private async void  Button_Clicked_1(object sender, EventArgs e)
        {
            var column = (Column)BindingContext;
            column.StationName = entry.Text;
            column.Colour = "Red";
            await App.Datas.InsertData(column);
            listView.ItemsSource = await App.Datas.GetDataAsync();
        }

    }
}

using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Xamarin.Facebook.Login.Widget;
using System.Collections.Generic;
using Xamarin.Facebook;
using Java.Lang;
using Firebase.Auth;
using Firebase;
using Xamarin.Facebook.Login;
using Android.Gms.Tasks;
using Android.Content;
using Xamarin.Auth;
using System;
using Newtonsoft.Json;

namespace LoginFB
{
    public class Data
    {
        public string Url { get; set; }
    }
    public class Picture
    {
        public Data Data { get; set; }
    }
    public class Cover
    {
        public string Id { get; set; }
        public int OffsetY { get; set; }
        public int Source { get; set; }
    }

    public class FacebookUser
    {
        public string Name { get; set; }
        public Picture Picture { get; set; }

        public Cover Cover { get; set; }

        public string Id { get; set; }

    }



    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, IFacebookCallback,IOnSuccessListener,IOnFailureListener
    {
        TextView user;
        TextView email;
        TextView photo;
        LoginButton loginButton;
        ICallbackManager callbackManager;
        FirebaseAuth firebaseAuth;
        private bool usingFirebase;
        Button fblogin;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
         
            SetContentView(Resource.Layout.activity_main);

            user = FindViewById<TextView>(Resource.Id.user);
            email = FindViewById<TextView>(Resource.Id.email);
            photo = FindViewById<TextView>(Resource.Id.photo);
            loginButton = FindViewById<LoginButton>(Resource.Id.loginbtn);
            fblogin = FindViewById<Button>(Resource.Id.fbBtn);
            loginButton.SetReadPermissions(new List<string> { "public_profile", "email" });

            callbackManager = CallbackManagerFactory.Create();

            loginButton.RegisterCallback(callbackManager, this);

            firebaseAuth = GetFirebaseAuth();

            fblogin.Click += Fblogin_Click;

        
        }

        private void Fblogin_Click(object sender, System.EventArgs e)
        {
            var auth = new OAuth2Authenticator("603715576915790", "",
                                                new System.Uri("https://m.facebook.com/dialog/oauth/"),
                                                new Uri("http://m.facebook.com/connect/login_successfull.html"));


            //        var auth = new OAuth2Authenticator(
            //         "603715576915790",
            //         "email",
            //new Uri("https://accounts.google.com/o/oauth2/v2/auth"),
            //new Uri("https://www.googleapis.com/oauth2/v4/token"),
            //isUsingNativeUI: true);



            //var v = new OAuth1Authenticator("603715576915790","ca8be450e8b00adcff601faaa6788546")

            auth.Completed += Auth_Completed;

            var ui = auth.GetUI(this);

            StartActivity(ui);
                                                       
                
        }
                
                
      
                
            



           

        private async void Auth_Completed(object sender, AuthenticatorCompletedEventArgs e)
        {

            if(e.IsAuthenticated)
            {

            var request = new OAuth2Request(
                "GET",
                new System.Uri("https://graph.facebook.com/me?fields=name,picture,cover,birthday"
                ),
                null,
                e.Account);

            var fbResponse = await request.GetResponseAsync();
            var json = fbResponse.GetResponseText();

                var fbData = JsonConvert.DeserializeObject<FacebookUser>(json);

                user.Text = fbData.Name;

                

                this.Finish();
            }
            else
            {
                this.Finish();
                Toast.MakeText(this, "Login Failed", ToastLength.Short).Show();
            }
        }

        FirebaseAuth GetFirebaseAuth()
        {
            var app = FirebaseApp.InitializeApp(this);
            FirebaseAuth mauth;

            if(app == null)
            {
                var option = new FirebaseOptions.Builder()
                             .SetProjectId("loginwithfacebook-a7455")
                             .SetApplicationId("loginwithfacebook-a7455")
                             .SetApiKey("AIzaSyAUjuP4EuGlyPjhlB_LTwgtBJkWPnd0Gd4")
                             .SetDatabaseUrl("https://loginwithfacebook-a7455.firebaseio.com")
                             .SetStorageBucket("loginwithfacebook-a7455.appspot.com").Build();
                app = FirebaseApp.InitializeApp(this, option);
                mauth = FirebaseAuth.Instance;

            }
            else
            {
                mauth = FirebaseAuth.Instance;
            }

            return mauth;
        }


        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            callbackManager.OnActivityResult(requestCode, (int)resultCode, data);
        }

        public void OnCancel()
        {
           
        }

        public void OnError(FacebookException error)
        {
            
        }

        //public void OnSuccess(Java.Lang.Object result)
        //{
        //    if(!usingFirebase)
        //    {

        //    usingFirebase = true;
        //    LoginResult loginResult = result as LoginResult;
        //    var credential = FacebookAuthProvider.GetCredential(loginResult.AccessToken.Token);
        //    firebaseAuth.SignInWithCredential(credential).AddOnSuccessListener(this)
        //                 .AddOnFailureListener(this);
        //    }
        //    else
        //    {
        //        Toast.MakeText(this, "Login SuccessFull", ToastLength.Short).Show();
        //        email.Text = firebaseAuth.CurrentUser.Email;
        //        photo.Text = firebaseAuth.CurrentUser.PhotoUrl.Path;
        //        user.Text = firebaseAuth.CurrentUser.DisplayName;
        //    }
        //}

        public void OnSuccess(Java.Lang.Object result)
        {
            throw new NotImplementedException();
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            throw new NotImplementedException();
        }
    }
}
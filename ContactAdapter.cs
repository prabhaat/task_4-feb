﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace contentProvider
{
    class ContactAdapter : BaseAdapter
    {

        List<Contacts> contactlist;
        Activity activity;


        public ContactAdapter(Activity activity)
        {
            this.activity = activity;

            GetContacts();
        }


        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return contactlist[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView ?? activity.LayoutInflater
                                      .Inflate(Resource.Layout.DataLayout, parent, false);


            var contactName = view.FindViewById<TextView>(Resource.Id.contactId);

            contactName.Text = contactlist[position].Name;


            return view;
        }


        void GetContacts()
        {
            var uri = ContactsContract.Contacts.ContentUri;

            string[] data =
            {
                ContactsContract.Contacts.InterfaceConsts.Id,
                ContactsContract.Contacts.InterfaceConsts.DisplayName

            };

            var loader = new CursorLoader(activity, uri, data, null, null,null);
            
            var cursor = (ICursor)loader.LoadInBackground();

            contactlist = new List<Contacts>();

            if(cursor.MoveToFirst())
            {
                do
                {
                    contactlist.Add(new Contacts
                    {
                        Id = cursor.GetLong(cursor.GetColumnIndex(data[0])),
                        Name = cursor.GetString(cursor.GetColumnIndex(data[1]))
                    });
                }
                while (cursor.MoveToNext());
            }


        }


        //Fill in cound here, currently 0
        public override int Count
        {
            get
            {
                return 0;
            }
        }

    }

    class ContactAdapterViewHolder : Java.Lang.Object
    {
        //Your adapter views to re-use
        //public TextView Title { get; set; }
    }
}
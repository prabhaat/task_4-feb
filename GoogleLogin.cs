using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Gms.Auth.Api.SignIn;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Common.Apis;
using Android.Gms.Common;
using System;
using Firebase.Auth;
using Firebase;
using Android.Gms.Tasks;
using Firebase.Messaging;
using Firebase.Iid;
using Android.Util;

namespace googleSignInExample
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity,GoogleApiClient.IOnConnectionFailedListener,IOnSuccessListener,IOnFailureListener
    {
        static readonly string Tag = "MainActivity";
        internal static readonly string ChannelId = "notification_Cahnnel";
        internal static readonly int NotificationId = 100;


        Button loginBtn;
        GoogleSignInOptions googleSignIn;
        GoogleSignInClient mGoogleSignInClient;
        GoogleApiClient mgoogleApiClient;
        FirebaseAuth firebaseAuth;
        TextView msgTxt;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            
            // For Data Accompanying the Notification
            //They are Saved in Intent.Extras
            if(Intent.Extras != null)
            {
                foreach(var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Log.Debug(Tag, "Key:{0} value: {1}", key, value);
                }
            }

            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
           
            loginBtn = FindViewById<Button>(Resource.Id.loginBtn);
            
            msgTxt = FindViewById<TextView>(Resource.Id.msgText);



            IsPlayServiceAvailable();
          
            CreateNotificationChannel();


            //Configuiring SignIn Option
            googleSignIn = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
                           .RequestIdToken("937676266937-gqlum3ogjemk23po7418abkrban9p5tt.apps.googleusercontent.com")
                           .RequestEmail().Build();

            
            // Will Communicate to google's Api and call google's intent

            mgoogleApiClient = new GoogleApiClient.Builder(this).EnableAutoManage(this, this)
                              .AddOnConnectionFailedListener(this).AddApi(Auth.GOOGLE_SIGN_IN_API, googleSignIn).Build();

            mgoogleApiClient.Connect();


          //  firebaseAuth = GetFirebaseAuth();

           // GoogleSignInAccount account = GoogleSignIn.GetLastSignedInAccount(this);
                                     
            

            loginBtn.Click += LoginBtn_Click;
            
            var logTokenButton = FindViewById<Button>(Resource.Id.logTokenButton);
           
            // For Display of Instance ID if Required 
            logTokenButton.Click += delegate {
                Log.Debug(Tag, "InstanceID token: " + FirebaseInstanceId.Instance.Token);
            };
        }







       //==================================
       // Login Button Click Event Listner 
       //==================================
        private void LoginBtn_Click(object sender, System.EventArgs e)
        {
            SignIn();
        }


        
        //=============================================
        // MAIN FUNCTION THAT OPENS GMAIL INTENT FOR 
        //=============================================
        public void SignIn()
        {
            var intent = Auth.GoogleSignInApi.GetSignInIntent(mgoogleApiClient);
            StartActivityForResult(intent,2020);
            

        }



        //============================================================
        //      THE RESULT FROM GOOGLE INTENT OPENED IN SIGNIN METHOD
        //============================================================
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == 2020)
            {
                var result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);


                var check = result.IsSuccess;

                //var signin = result.Status;
                var account = result.SignInAccount;

                string txt = account.DisplayName;

                Toast.MakeText(this, "welcome " + txt, ToastLength.Long).Show();


            }
        }



        //================================================
        // CHECK IF MESSAGING SERVICE IS AVAILABLE OR NOT
        //================================================
        public bool IsPlayServiceAvailable()
        {
            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
              
            if(resultCode != ConnectionResult.Success)
            {
                if(GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    msgTxt.Text = GoogleApiAvailability.Instance
                                  .GetErrorString(resultCode);
                }
                else
                {
                    msgTxt.Text = "This Device is not Supported";
                    Finish();
                }
                return false;
            }
            else
            {
                msgTxt.Text = "google Play services Available";
                return true;
            }
        }


        //==================================
        // CREATE CUSTOM NOTIFICATION CHANNEL
        //==================================

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
               
                return;
            }

            var channel = new NotificationChannel(ChannelId,"Push Notifications",NotificationImportance.Default)
            {

                Description = "Firebase Cloud Messages appear in this channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }


        //======================================================
        /*    
         *      FIREBASE AUTH METHOD
         *     REQUIRED ONLY TO PUSH DATA TO FIREBASE DATABASE
         */
         //======================================================
        FirebaseAuth GetFirebaseAuth()
        {
            var app = FirebaseApp.InitializeApp(this);
            FirebaseAuth firebaseAuth;

            if (app == null)
            {
                var option = new FirebaseOptions.Builder().SetProjectId("loginexample-429b0").SetApplicationId("loginexample-429b0")
                             .SetApiKey("AIzaSyDrG8-bNU1TSVw-naWrZu5m674PwDG6ES0").SetDatabaseUrl("https://loginexample-429b0.firebaseio.com")
                             .SetStorageBucket("loginexample-429b0.appspot.com")
                             .Build();

                app = FirebaseApp.InitializeApp(this, option);
                firebaseAuth = FirebaseAuth.Instance;
            }
            else
            {
                firebaseAuth = FirebaseAuth.Instance;
            }
            return firebaseAuth;
        }

       

        //=====================================
        // Implementation of interface
        //=====================================

        public void OnConnectionFailed(ConnectionResult result)
        {
            throw new System.NotImplementedException();
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            Toast.MakeText(this, "Successfull Login", ToastLength.Long).Show();
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            Toast.MakeText(this, "Error", ToastLength.Long).Show();
            throw new NotImplementedException();
        }
    }
}

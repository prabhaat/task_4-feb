﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



//Task 1 in camelCase Format

//contains variable that are common and required by inherited classes
class conversion   
{
    public int numberInt;
    public float numberFloat;
    public char charaterChar;
    public double numberDouble;


}

// class that contains methods of type conversion of float.
class toFloat : conversion       
{
    //method that contains type conversion float.
    public void conversion(float numberFloat)                       
    {
        Console.WriteLine("below are the conversion of float type:");

        Console.WriteLine("float to int " + (int)Float);

        Console.WriteLine("float to double " + (double)Float);

    }
}

//class contain method of type conversion of integer
class toInt : toFloat     
{
    //function for different conversion of Int type variable
    public void conversion(int numberInt)
    {
        Console.WriteLine("below are the conversion of Integer type:");
        numberFloat = numberInt;                                                      //implicit conversion
        Console.WriteLine("integer to float " + numberFloat);
        numberDouble = numberInt;                                                     //implicit conversion

        Console.WriteLine("integer to double " + numberDouble);

        characterChar = Convert.ToChar(numberInt);                                      //explict
        Console.WriteLine("Integer to char " + characterChar);
    }
}

//for conversion of char to integer
class toChar : toInt     
{
    //function for different conversions of char type variable
    public void conversion(char characterChar)
    {
        Console.WriteLine("below are the conversion of char type:");
        numberInt = characterChar;                                               //implicit
        Console.WriteLine("char to int " + numberInt);
        numberDouble = characterChar;                                            //implicit
        Console.WriteLine("char to double " + numberDouble);
    }
}

//class for conversion of String
class Task1 : toChar    
{

    //function to for different conversion of string type variable
    public void conversion(string stringValue)
    {
        Console.WriteLine("below are the conversion of String type:");

        numberInt = Convert.ToInt32(stringValue);   // explicit

        Console.WriteLine("String to int " + numberInt);

    }
}

//conversion method will work as overloaded


//also an example of runtime polimorphism


namespace firstapp
{
    class Program
    {
        static void Main(string[] args)
        {
            float numberFLoat = 3f;
            Task1 task1 = new Task1();
            task1.conversion("1234");
            task1.conversion(2);
            task1.conversion('a');
            task1.conversion(FLoat);
            Console.ReadKey();
        }
    }
}

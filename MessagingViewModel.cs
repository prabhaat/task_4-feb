﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace MessagingCenterExample
{
    class MessagingViewModel
    {
        public ObservableCollection<string> Data { get; set; }

        public MessagingViewModel()
        {
            Data = new ObservableCollection<string>();

            MessagingCenter.Subscribe<MainPage>(this, "Hi", (sender) =>
            {
                Data.Add("Hi");
            });

            MessagingCenter.Subscribe<MainPage, string>(this, "Hi", (sender, arg) =>
            {
                Data.Add("Hi " + arg);
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Firebase.Messaging;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Support.V4.App;

namespace googleSignInExample
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string Tag = "FCM";
        static bool again = false;
        //====================================================
        //      ON GETTING MESSAGE FROM REMOTE SERVER
        //====================================================
        public override void OnMessageReceived(RemoteMessage message)
        {
            Log.Debug(Tag, "From: " + message.From);
            Log.Debug(Tag, "Notification Message Body: " + message.GetNotification().Body);
            Log.Debug(Tag, "message Id " + message.MessageId);

            foreach (var keypair in message.Data)
            {
                Log.Debug(Tag, "Key:" + keypair.Key);
                Log.Debug(Tag, "Value:" + keypair.Value);
            }

            Log.Debug(Tag, "Check for method Calling");

            SendNotification(message.GetNotification().Body, message.Data);

            Log.Debug(Tag, "Check for method Calling LAter");



        }


        void SendNotification(string message, IDictionary<string, string> data)
        {
            if (!again)
            {

                var intent = new Intent(this, typeof(SecondActivity));
                intent.AddFlags(ActivityFlags.ClearTop);
                foreach (var key in data.Keys)
                {
                    intent.PutExtra(key, data[key]);
                }



                var pendingIntent = PendingIntent.GetActivity(this, MainActivity.NotificationId, intent, PendingIntentFlags.UpdateCurrent);


                var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.ChannelId)
                                         .SetSmallIcon(Resource.Drawable.ic_mtrl_chip_checked_circle)
                                         .SetContentTitle("FCM Message")
                                         .SetContentText(message)
                                         .SetAutoCancel(true)
                                         .SetContentIntent(pendingIntent);

                var notificationManager = NotificationManagerCompat.From(this);
                notificationManager.Notify(MainActivity.NotificationId, notificationBuilder.Build());
            }
            else
            {
                Intent intent = new Intent(this, typeof(PowerNapActivity));
                intent.AddFlags(ActivityFlags.ClearTop);
                foreach (var key in data.Keys)
                {
                    intent.PutExtra(key, data[key]);
                }



                var pendingIntent = PendingIntent.GetActivity(this, MainActivity.NotificationId, intent, PendingIntentFlags.UpdateCurrent);


                var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.ChannelId)
                                         .SetSmallIcon(Resource.Drawable.ic_mtrl_chip_checked_circle)
                                         .SetContentTitle("FCM Message")
                                         .SetContentText(message)
                                         .SetAutoCancel(true)
                                         .SetContentIntent(pendingIntent);

                var notificationManager = NotificationManagerCompat.From(this);
                notificationManager.Notify(MainActivity.NotificationId, notificationBuilder.Build());
            }
            
            again = true;

            Log.Debug(Tag, "Check for method Calling send");
        }

    }
}
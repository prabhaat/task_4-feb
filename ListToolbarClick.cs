using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using Android.Widget;
using LoginScreenAndroidApp.Resources;

namespace LoginScreenAndroidApp
{
    [Activity(Label = "newLayout", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class ChangePageActivity : AppCompatActivity
    {
        public static int ResultCode = 2;

        TextView edit;
        ObservableCollection<Information> data = new ObservableCollection<Information>
               {
                new Information{UpdateDate = "Prabhat",Id= 0,Time="21",By="123@123.cc"},
                new Information{UpdateDate = "arif",Id= 1,Time="21",By="123@123.cc"},
                new Information{UpdateDate= "Ashish",Id= 2,Time="21",By="123@123.cc"},
                new Information{UpdateDate = "Ayush",Id= 3,Time="21",By="123@123.cc"}
               };

        ListView globalListView;
        CustomAdapter custom;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            //await TryToGetPermission();

            base.OnCreate(savedInstanceState);

            edit = FindViewById<EditText>(Resource.Id.txtvew);
            // Create your application here

            SetContentView(Resource.Layout.NewLayout);

            //android.support.v7.widget.toolbar tool = findviewbyid<android.support.v7.widget.toolbar>(resource.id.toolbar1);
            //setsupportactionbar(tool);

            Android.Support.V7.Widget.Toolbar tool = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar1);


            SetSupportActionBar(tool);

            Button b = FindViewById<Button>(Resource.Id.button1); 
            ImageButton btn = FindViewById<ImageButton>(Resource.Id.imgbtn);

           

            // TextView text = FindViewById<TextView>(Resource.Id.textWelcome);

            // string usermane = Intent.GetStringExtra("userName");

            //  text.Text = "Welcome " + usermane;

            //  string text = "<font color = #FF0000>Hello </font> <font color = #FF0FFFF> world</font>";

            //this.edit.SetText(Html.FromHtml(GetString(Resource.String.myText),FromHtmlOptions.ModeCompact);


            var listVew = FindViewById<ListView>(Resource.Id.listView);

            globalListView = FindViewById<ListView>(Resource.Id.listView);


            btn.Click += Btn_Click;

           
            
            b.Click += (obj, e) =>
            {

                //Intent i = new Intent(this, typeof(PersonalActivity));

                //StartActivity(i);

                //StartActivityForResult(typeof(PersonalActivity), ResultCode);



                var adapter = new CustomAdapter(this, data);

                custom = new CustomAdapter(this, data);
                //listVew.Adapter = adapter;

                globalListView.Adapter = custom;

                globalListView.ItemClick += OnItemClicked;
            };



        }


        //================================
        // TOOL BAR IMAGE BUTTON CLICK HANDLED
        // =====================================
        private void Btn_Click(object sender, EventArgs e)
        {

            LayoutInflater inflater = LayoutInflater.From(Application.Context);

            var alertView = inflater.Inflate(Resource.Layout.AddRecord, null);

            var date = alertView.FindViewById<EditText>(Resource.Id.datadate);
            var by = alertView.FindViewById<EditText>(Resource.Id.databy);
            var time = alertView.FindViewById<EditText>(Resource.Id.datatime);
            //var from = alertView.FindViewById<EditText>(Resource.Id.datafrom);
            //var to = alertView.FindViewById<EditText>(Resource.Id.datato);

            Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);

            builder.SetView(alertView);
            Android.App.AlertDialog dialog = builder.Create();
            dialog.SetTitle("Add Record");
            dialog.SetButton("Save", (s, e) =>
            {
                if(!string.IsNullOrEmpty(date.Text) && !string.IsNullOrEmpty(by.Text) && !string.IsNullOrEmpty(time.Text))
                  data.Add(new Information { By = by.Text, Time = time.Text,Id = 8 ,UpdateDate = date.Text });

               custom.NotifyDataSetChanged();

            });

            dialog.SetButton2("Cancel", (s, e) =>
            {
                return;
            });

            dialog.Show();

        }


        //=========================================================================
        // List View Click Event Handled
        //=========================================================================
        private void OnItemClicked(object sender, AdapterView.ItemClickEventArgs e)
        {
            var obj = (ListView)sender;
            var view = e.View;

            //TextView txt = FindViewById<TextView>(Resource.Id.changesMode);

            var txtStatus = view.FindViewById<TextView>(Resource.Id.statusChanged);



            LayoutInflater inflater = LayoutInflater.From(Application.Context);

            var alertView = inflater.Inflate(Resource.Layout.AlertLayout, null);

            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);

            alert.SetView(alertView);




            Android.App.AlertDialog alertDialog = alert.Create();

            alertDialog.SetTitle("Testing");

            alertDialog.SetButton("update", (sender, e) =>
            {
                var to = alertView.FindViewById<EditText>(Resource.Id.alertTo);
                var from = alertView.FindViewById<EditText>(Resource.Id.alertFrom);

                if(!string.IsNullOrEmpty(to.Text) && !string.IsNullOrEmpty(from.Text))
                 txtStatus.Text = "Status Changed From " + from.Text.ToString() + " to " + to.Text.ToString();

            });

            alertDialog.SetButton2("Cancel", (sender, e) =>
            {
                return;
            });

            alertDialog.Show();
            // data[e.Position].By = "Hello world";

            // custom.NotifyDataSetChanged();
       
        }







        //public override bool (imenu menu)
        //{
        //    var inflater = menuinflater;
        //    inflater.inflate(resource.layout.menulayout, menu);
        //    return base.oncreateoptionsmenu(menu);
        //}





    //================================================
    // DEVICE PERMISSION 
    //==============================================

        #region permisson
        async Task TryToGetPermission()
        {
            if ((int)Build.VERSION.SdkInt >= 23)
            {
                await GetPermission();
                return;
            }
        }

        const int RequestLocationId = 0;

        readonly string[] PermissionGroupLocation =
       {
            Manifest.Permission.AccessCoarseLocation,
            Manifest.Permission.AccessFineLocation
        };

        async Task GetPermission()
        {
            string permission = Manifest.Permission.AccessFineLocation;

            if (CheckSelfPermission(permission) == (int)Android.Content.PM.Permission.Granted)
            {
                Toast.MakeText(this, "Loaction Permission Granted", ToastLength.Short).Show();
                return;
            }

            if (ShouldShowRequestPermissionRationale(permission))
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                alert.SetTitle("Permissions Needed");
                alert.SetMessage("The application need special permissions to continue");
                alert.SetPositiveButton("Request Permissions", (senderAlert, args) =>
                {
                    RequestPermissions(PermissionGroupLocation, RequestLocationId);
                });

                alert.SetNegativeButton("Cancel", (senderAlert, args) =>
                {
                    Toast.MakeText(this, "Cancelled!", ToastLength.Short).Show();
                });

                Dialog dialog = alert.Create();
                dialog.Show();


                return;
            }

            RequestPermissions(PermissionGroupLocation, RequestLocationId);
        }

        public override async void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            switch (requestCode)
            {
                case RequestLocationId:
                    {
                        if (grantResults[0] == (int)Android.Content.PM.Permission.Granted)
                        {
                            Toast.MakeText(this, "Special permissions granted", ToastLength.Short).Show();

                        }
                        else
                        {

                            Toast.MakeText(this, "Special permissions denied", ToastLength.Short).Show();

                        }
                    }
                    break;
            }

        }

        #endregion




        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            var inflater = MenuInflater;
            inflater.Inflate(Resource.Menu.MenuLayout, menu);
            return base.OnCreateOptionsMenu(menu);
        }


        //public override bool OnOptionsItemSelected(IMenuItem item)
        //{

        //        Int id = item.ItemId; // getting i.d of the selected item  
        //        If(id == Resource.Id.search) {
        //            Toast.MakeText(this, ”Search clicked”, ToastLength.Short).Show();
        //            return true;
        //        }
        //        If(id == Resource.Id.share) {
        //            Toast.MakeText(this, ”Share clicked”, ToastLength.Short).Show();
        //            return true;
        //        }
        //        If(id == Resource.Id.email) {
        //            Toast.MakeText(this, ”Email clicked”, ToastLength.Short).Show();
        //            return true;
        //        }
        //        Return base.OnOptionsItemSelected(item);



        //}

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            edit = FindViewById<TextView>(Resource.Id.txtvew);
            // base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == ResultCode)
                if (resultCode == Result.Ok)
                    edit.Text = data.Data.ToString();
        }
    }
}
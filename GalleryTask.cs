using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Provider;
using Android.Content;
using Android.Graphics;
using System.IO;
//using Android.Media;
//using System.Drawing;

namespace GalleryApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ImageView imageView;
        Button capture;
        Button upload;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            imageView = FindViewById<ImageView>(Resource.Id.imageview);
          
            capture = FindViewById<Button>(Resource.Id.capturebutton);

            upload = (Button)FindViewById(Resource.Id.uploadbutton);

            upload.Click += Upload_Click;

            capture.Click += Capture_Click;

        }

        private void Upload_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);
            this.StartActivityForResult(Intent.CreateChooser(intent, "Select Image"), 10);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            
            if(requestCode == 10)
            {
                if(resultCode == Result.Ok)
                {
                    Stream stream = ContentResolver.OpenInputStream(data.Data);

                   imageView.SetImageBitmap(BitmapFactory.DecodeStream(stream));

                }
            }
            else
            {
                 Bitmap bitmap = (Bitmap)data.Extras.Get("data");
                 imageView.SetImageBitmap(bitmap);

            }

        }

        private void Capture_Click(object sender, System.EventArgs e)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            StartActivityForResult(intent, 0);
            
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MessagingCenterExample
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MessagingViewModel();

            MessagingCenter.Subscribe<MainPage, string>(this, "hi", async (sender, arg) =>
            {
                await DisplayAlert("Message", "Message Recieved", "Ok");
            });
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            MessagingCenter.Send<MainPage>(this, "Welcome");
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            MessagingCenter.Send<MainPage, string>(this, "Hello,", "How are you");
        }
        
       async private void Unsubscribe(object sender, EventArgs e)
        {
            MessagingCenter.Unsubscribe<MainPage, string>(this, "Welcome");
            await DisplayAlert("Message", "Unsubscribbed", "Ok");
        }
    }
}

using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Gms.Auth.Api.SignIn;
using Android.Content;
using Android.Gms.Auth.Api;
using Android.Gms.Common.Apis;
using Android.Gms.Common;
using System;

namespace googleSignInExample
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity,GoogleApiClient.IOnConnectionFailedListener
    {
        Button loginBtn;
        GoogleSignInOptions googleSignIn;
        GoogleSignInClient mGoogleSignInClient;
        GoogleApiClient mgoogleApiClient;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            loginBtn = FindViewById<Button>(Resource.Id.loginBtn);

            googleSignIn = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
                           .RequestIdToken("988432346432-5t2t33fa60o11oqksqiilldavomjgi0k.apps.googleusercontent.com")
                           .RequestEmail().Build();

            //mGoogleSignInClient = GoogleSignIn.GetClient(this, googleSignIn);

            mgoogleApiClient = new GoogleApiClient.Builder(this).EnableAutoManage(this, this)
                              .AddOnConnectionFailedListener(this).AddApi(Auth.GOOGLE_SIGN_IN_API, googleSignIn).Build();

            mgoogleApiClient.Connect();
                                

            GoogleSignInAccount account = GoogleSignIn.GetLastSignedInAccount(this);
                                     
            

            loginBtn.Click += LoginBtn_Click;
        }

       //==================================
       // Login Button Click Event Listner 
       //==================================
        private void LoginBtn_Click(object sender, System.EventArgs e)
        {
            SignIn();
        }

        public void SignIn()
        {
            Intent intent = Auth.GoogleSignInApi.GetSignInIntent(mgoogleApiClient);
            StartActivityForResult(intent,2020);
            

        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
           if(requestCode == 2020)
            {
                var result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);

                     
                var check = result.IsSuccess;

                //var signin = result.Status;
                var account = result.SignInAccount;

                //string txt = account.DisplayName;
                   
                Toast.MakeText(this, "welcome ", ToastLength.Long).Show();
                     
            
            }
        }


        public void OnConnectionFailed(ConnectionResult result)
        {
            throw new System.NotImplementedException();
        }
    }
}

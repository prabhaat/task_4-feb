﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace LoginScreenAndroidApp
{

    

    [Activity(Label = "Personal")]
    public class PersonalActivity : Activity
    {
        EditText edit;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.PersonalDetail);

            edit = FindViewById<EditText>(Resource.Id.edtxt);

            Button btn = FindViewById<Button>(Resource.Id.NextBtn);
            Button btn2 = FindViewById<Button>(Resource.Id.canBtn);


            Intent intent = new Intent(this,typeof(MainActivity));



            btn.Click += (obj, e) =>
            {
                //this.Finish();
                Intent intent1 = new Intent();
                intent1.SetData(Android.Net.Uri.Parse(edit.Text));
                SetResult(Result.Ok, intent1);
                this.Finish();
            };

            btn2.Click += (obj, e) =>
            {
                intent.AddFlags(ActivityFlags.ClearTop);
               
                StartActivity(intent);

            };


           

            
        }
    }
}
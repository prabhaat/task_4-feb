﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



// Task in camelCase




//class containing variables and methods 
public class Vehicle
{
    string vehicleName;     //name of the vehicle

    string vehicleColor;           //color of vehicle

    //setting property of variable color
    //since color is private we cannot access it directly on object
    //so we use property method provided in c# to set its value
    //property setColor to set the value of color of the vehicle
    public string SetColor
    {
        get { return vehicleColor; }
        set
        {
            vehicleColor = value;
        }
    }

    int vehicleWheel;

    //using property set wheel to set the value of wheel since it is private
    public int SetWheel
    {
        get { return vehicleWheel; }
        set
        {
            vehicleWheel = value;
        }
    }

    int speed = 0;                     //initial speed 0

    bool isStop = true;                //currently car is stopped

    public const int MaxSpeed = 300;   //limit of max speed



    //constructor will set the name of the vehicle
    //since constructor can be invoked once per object 
    //pros : the name will be attched to the object
    public Vehicle(string vehicleName)
    {
        this.vehicleName = vehicleName;
    }

    //method to start the car 
    public void Start()
    {
        //information of car
        Console.WriteLine("your {0} colour {1} having {2} wheels has been started \nspeedup" +
                           "to move forward", color, vehicleName, wheel);

        speed = 0;       //since car is started so initial value 0

        isStop = false;   //setting the value of if car is stopped or not as false    
    }
    public void Stop()
    {
        //information of car
        Console.WriteLine("your {0} colour {1} having {2} wheels has been stopped", color, vehicleName, wheel);

        speed = 0;            //stopped speed 0

        isStop = true;       //car is stopped so the variable is true
    }



    //Method will speed up the car by adding provided speed to the current speed of vehicle
    public void SpeedUp(int speed)          //integer type parameter, argument is speed by which 
    {                                       //we are increasing current speed
        //if car is stopped we can't increase speed
        //can't have negative value of speed 
        if (!isStop && speed >= 0)
        {
            //making sure that speed is less then maxspeed
            if (this.speed <= MaxSpeed)
            {
                Console.WriteLine("speed of your {0} having {1} wheel has been incresed by" +
                                    " {2}", vehicleName, wheel, speed);

                this.speed = this.speed + speed;     //increasing speed

                //if speed is greater than maxSpeed 
                if (this.speed > MaxSpeed)
                    this.speed = MaxSpeed;

                Console.WriteLine("current speed of your vehicle is {0}", this.speed);
            }
        }
        if (speed < 0)
        {
            Console.WriteLine("invalid speed, please enter the positive integer value");
        }

    }

}

namespace Task
{

    class Program
    {
        static void Main(string[] args)
        {

            Vehicle car = new Vehicle("civic");      //making object of Vehicle
            car.SetColor = "blue";                   //using property setcolor
            car.SetWheel = 4;                        //setting number of wheel 
            car.Start();                             //start method
            car.SpeedUp(20);                         //speed method
            car.SpeedUp(10);
            car.Stop();
            car.SpeedUp(50);
            Console.WriteLine("\n");
            Console.ReadKey();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Firebase.Iid;
using Android.Util;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace googleSignInExample
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "MyFirebaseIIDService";


        //Generated Token on Refresh
        public override void OnTokenRefresh()
        {
            var refreshToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed Token: " + refreshToken);
            SendRegistrationToServer(refreshToken);
        }

        void SendRegistrationToServer(string refreshToken)
        {
            
        }
    }
}
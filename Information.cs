﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace LoginScreenAndroidApp
{
   public class Information
    {
        public int Id { get; set; }
        public string UpdateDate { get; set; }
        public string By { get; set; }
        public string Time { get; set; }
    }
}